## Overview ##

ChequeredPy aims to standardise the protocol used for racing lights so that costs can be brought down to improve  driver safety.

This examplar example is intended to be built on an Arduino with a Raspberry Pi running Python on as a controller.

## Data structure -- Flags ##
The data structure is based on JSON dictionaries to increase cross-platform languages
```
#!python

{
    'track': 'Winton', #The track as per the main website database
    'category': 'flag_point', #Category of the message. Intent is to expand this to include vehicle telemetry and timing
    'identifier': '0', #Flag point number as registered to the website. Start/Finish is point 0.
    'action': 'change', #Action to be performed either: change a flag state, register a flag
    'data': 'yellow', #Flag colour
    }
```

## Website ##
The website code remains private, but some indicative screenshots are below:
 
![screenshot_winton.jpg](https://bitbucket.org/repo/yp57b8y/images/1651739734-screenshot_winton.jpg)