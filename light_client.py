#!/usr/bin/env python
'''
Aim is to simulate sending to flag points
'''
import pika, time, urllib, urllib2,json
import logging
from threading import Thread
from light_handler import LightHandler
from types import *

FORMAT = '%(asctime)-15s -- %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(0)

class WebMessageConsumer():
    '''
    A class to handle messages to and from the race control web interface
    '''

    def __init__(self, queue, n, b, p, user, pas, logger=None):
        '''
        Initiates connection to the message broker and setup logging

        :param queue: Queue to listen on
        :param n: Name of the device
        :param b: Broker to listen too
        :param p: Port to listen on
        '''
        self.CREDENTIALS = pika.PlainCredentials(user, pas)
        self.PARAMETERS = pika.ConnectionParameters(host=b, port=p, virtual_host='/',
                                                    credentials=self.CREDENTIALS)
        self.QUEUE = queue
        self._ID = n
        self.light_handler = LightHandler()
        self.logger = logger or logging.getLogger('lights.web.{0}'.format(self._ID))
        #self.register()
        self.consume_state_change()
        #self.lcd = nanpy.Lcd([7,8,9,10,11,12],[16,2])

    def register(self):
        '''
        Registers devices in the models database
        :return:
        '''
        data = urllib.urlencode({'name' : self._ID,
                                  'lat' : 1,
                                  'long' : 2,
                                  'track' : self.QUEUE})
        content = urllib2.urlopen(url=self.REGISTER, data=data).read()
        return

    def consume_state_change(self):
        '''
        Start consuming messaging from queue identified in
        :return:
        '''
        self.connection = pika.BlockingConnection(self.PARAMETERS)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange=self.QUEUE, type='direct', durable=True)
        self.result = self.channel.queue_declare(exclusive=True)
        self.queue_name = self.result.method.queue
        self.channel.queue_bind(exchange=self.QUEUE, queue=self.queue_name, routing_key=self._ID)
        self.channel.queue_bind(exchange=self.QUEUE, queue=self.queue_name, routing_key='all')
        self.channel.basic_consume(self.state_change, queue=self.queue_name, no_ack=True)
        self.logger.warning('Connected and Listening - {0}'.format(self.PARAMETERS))
        self.channel.start_consuming()
        return

    def state_change(self, channel, method_frame, header_frame, body):
        '''
        This will control the flag colours based on the reported state.

        :param channel:
        :param method_frame:
        :param header_frame:
        :param body:
        :return:
        '''
        body_dict = json.loads(body)
        self.light_handler.change(body_dict['data'])
        self.logger.warning('{0} changed to {1}'.format(body_dict['identifier'], body_dict['data']))
        #self.lcd.printString('Flag State: {0}'.format(body))
        return

class WebMessageProducer():
    '''
    A class to handle messages to and from the race control web interface
    '''

    def __init__(self, queue, name, b, p, user, pas,logger=None):
        '''
        Initiates connection to the message broker

        :param queue: Queue to listen on
        :param name: Name of the device
        :param b: Broker to listen too
        :param p: Port to listen on
        '''
        self.CREDENTIALS = pika.PlainCredentials(user, pas)
        self.PARAMETERS = pika.ConnectionParameters(host=b, port=p, virtual_host='/',
                                                    credentials=self.CREDENTIALS)
        self.QUEUE = queue
        self._ID = name
        self.logger  = logger or logging.getLogger(__name__)
        self.light = LightHandler(self.logger)

    def publish_state_change(self, state):
        self.connection = pika.BlockingConnection(self.PARAMETERS)
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=self.QUEUE, durable=True, exclusive=False, auto_delete=False)
        self.channel.confirm_delivery()
        self.light.change(state)
        body_dict = {
            'track': self.QUEUE,
            'category': 'flag_point',
            'identifier': self._ID,
            'action': 'change',
            'data': state
        }
        if self.channel.basic_publish(exchange=self.QUEUE,
                                      routing_key='racecontrol',
                                      body=json.dumps(body_dict),
                                      properties=pika.BasicProperties(content_type='text/plain',
                                                                      delivery_mode=1),
                                      mandatory=True):
            self.logger.warning('Flag {0} State was changed - {1}'.format(body_dict['identifier'], body_dict['data']))
        else:
            self.logger.critical('Flag {0} State was not changed - {1}'.format(body_dict['identifier'], state))
        self.connection.close()


def register():
    '''
    This will register the light point in the database and either consume or update a new entry from DynamoDB
    :return:
    '''
    #TODO: Make a way to see if in databse. If so: set state; if not: create new entry based on parameters
    return

def initialise(q,n,b,p,user,pas,logger):
    '''
    This will start the device to consume and will allow production of messages
    '''
    register()
    consume = Thread(target=WebMessageConsumer,args=(q,n,b,p,user,pas,logger))
    publish = WebMessageProducer(q,n,b,p,user,pas,logger)
    consume.start()
    return publish

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Connect to an eRaceDirector messaging instance')
    parser.add_argument('q', nargs=1,
                        help='Specify queue to listen on (alsoo name of racetrack'
                        )
    parser.add_argument('n', nargs=1,
                        help='Specify a name for this device')
    parser.add_argument('b', nargs=1,
                        help='Specify a broker to listen on')
    parser.add_argument('p', nargs=1,
                        help='Specify a port to listen on')
    parser.add_argument('user', nargs=1,
                        help='Specify a username for queue')
    parser.add_argument('pas', nargs=1,
                        help='Specify a password for the queue')
    args = parser.parse_args()
    print '{0}-{1}'.format(args.q[0],args.n[0])
    p = initialise(args.q[0],args.n[0],args.b[0],int(args.p[0]),args.user[0],args.pas[0],logger)
    time.sleep(5)
    p.publish_state_change('yellow')
    while True:
        next = raw_input('Input state \n 0 - double yellow \n 1 - yellow \n 2- green \n 3 - blue: ')
        if next == "0":
            state = 'double_yellow'
        elif next == "1":
            state = 'yellow'
        elif next == "2":
            state = "green"
        elif next == "3":
            state = "blue"
        else:
            state = None
        if state:
            p.publish_state_change(state)

