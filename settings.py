'''
Sets defualt parameters for the light box.

Should be created as part of the puppet process if possible.
'''
import pika
CREDENTIALS = pika.PlainCredentials('light1','light2')
PARAMETERS = pika.ConnectionParameters(host='broker.skeogh.net',port=5672,virtual_host='/',credentials=CREDENTIALS)
QUEUE = 'wakefield'