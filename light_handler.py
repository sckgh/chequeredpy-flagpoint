'''
The aim of this module is to control lights.

The client will call the change function of LightHandler to changethea lights.

This module has been written to support 16x32 HUB75 LED displays
(https://www.aliexpress.com/item/DIP-p8-outdoor-full-color-led-display-module-256-128-mm-32-16-pixel-p8-rgb/32583541228.html)
and uses a PCDuino3 as the driver.
https://learn.sparkfun.com/tutorials/programming-the-pcduino/accessing-gpio-pins
'''
import logging, os, time

class LightHandler():
    '''
    A class to organise light changes
    '''

    def __init__(self,logger=None):
        '''
        TODO: Set-up light diagnostics
        '''
        self.state = 'clear'
        self.l = logger or logging.getLogger(__name__)
        self.l.warning('Lights Online')
        self.output = {}
        #HUB75 pins
        pin = ['R1','G1','B1','GND1','R2','G2','B2','GND2','A','B','C','D','CLK','LAT','OE','GND3']
        GPIO_MODE_PATH = os.path.normpath('/sys/devices/virtual/misc/gpio/mode/')
        GPIO_PIN_PATH = os.path.normpath('/sys/devices/virtual/misc/gpio/pin/')
        GPIO_FILENAME = "gpio"
        for i in range(2,9+2):
            with open(os.path.join(GPIO_MODE_PATH,GPIO_FILENAME+str(i))) as f:
                f.write("0")
            self.outputs[pin[i]] = os.path.join(GPIO_PIN_PATH,GPIO_FILENAME+str(i))

    def change(self,state):
        self.l.warning('Light changed - {0}'.format(self.state))
        if state == 'clear':
            self.state = 'clear'
            if self.state == 'yellow' or 'double_yellow':
                self.state = 'green'
                self.l.warning('Light changed - {0}'.format(self.state))
                #TODO: make green_then_clear
                self.state = 'clear'
            else:
                self.state = 'clear'
                #TODO make off
            for pin in self.output:
                f = open(pin)
                f.write("0")
                f.close()
        elif state == 'red':
            self.state = 'red'
            while True:
                f = open(self.output['R1'])
                f.write("1")
                f.close()
                f = open(self.output['R2'])
                f.write("1")
                f.close()
                time.sleep(1000)
                f = open(self.output['R1'])
                f.write("0")
                f.close()
                f = open(self.output['R2'])
                f.write("0")
                f.close()
                time.sleep(1000)
            #TODO: make red
        elif state == 'green':
            self.state = 'green'
            #TODO: make green
        elif state == 'double_yellow':
            self.state = 'double_yellow'
            #TODO: make double yellow
        elif state == 'yellow':
            self.state = 'yellow'
            #TODO: make yellow
        elif state == 'safety':
            self.state = 'safety'
            #TODO: make safety car
        elif state == 'blue':
            self.state = 'blue'
            #TODO: make blue
        elif state == 'white':
            self.state = 'white'
            #TODO: make white